{-# LANGUAGE OverloadedStrings #-}
module Main (main) where

import Graphics.Gloss
import System.Environment
import qualified System.Directory as D
import qualified Data.ByteString as B
import GHC.Word
import qualified Data.List as L

type Byte = Word8
type Data = [(Byte,Byte)]

grey = makeColor 0.2 0.2 0.2 1

main :: IO ()
main = do
        bytes <- B.readFile "LICENSE"--"src/Houzez.bmp"
        let from = 0
        let size = 2^19
        display (InWindow "Dibyte Visualiser" (500, 500) (200, 200)) black 
            (borderify . visualise . analyse . B.take size . B.drop from $ bytes)

borderify :: Picture -> Picture
borderify = mappend . color grey $ rectangleSolid 256 256

visualise :: Data -> Picture
visualise = color white
            . translate (-128) (128)
            . pictures 
            . fmap (\(x,y) -> translate (word82float x) (negate $ word82float y) 
                    $ rectangleSolid 2 2)
            . L.nub

analyse :: B.ByteString -> Data
analyse s = B.zip (B.drop 1 s) s

word82float :: Word8 -> Float
word82float = fromInteger . toInteger

char2word8 :: Char -> Word8
char2word8 = toEnum . fromEnum
